# Install project requirements
pip install -r requirements.txt

# Build staticfiles
python3.6 manage.py collectstatic --no-input