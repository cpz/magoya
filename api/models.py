from django.db import models


class Transaction(models.Model):
    TYPES = (("debit", "Debit"), ("credit", "Credit"))
    type = models.CharField(max_length=6, choices=TYPES, default="debit")
    created = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=100) # blank=False, unique=True)
    amount = models.FloatField()
    def __str__(self):
        return self.description