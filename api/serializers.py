from rest_framework import serializers
from .models import Transaction
from django.db.models import Sum

class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction
        fields = ['id','description','amount','type','created']

    def get_sum(self, type):
        list = Transaction.objects.filter(type=type)
        return list.aggregate(Sum('amount'))['amount__sum']

    def validate_amount(self, value):
        if(self.get_initial()['type'] == 'debit'):
            credit = self.get_sum('credit') or 0
            debit = self.get_sum('debit') or 0
            balance = credit - debit
            if (balance - value < 0):
                raise serializers.ValidationError("Insufficient funds")
        return value