import fetch from 'node-fetch'

const host = process.env.API_URL ?? ''
const endpoint = host + '/api/transactions/'

const handle = p =>p.then(res => [res,]).catch(err => Promise.resolve([, err]))

const getList = async () => await handle(fetch(endpoint).then(r=>r.json()))

const create = async data => await handle(fetch(endpoint, {
  method: 'POST',
  body: JSON.stringify(data),
  headers:{
    'Content-Type': 'application/json'
  }
}).then(r => r.json()))

export {
  create,
  getList
}
