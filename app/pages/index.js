import { useState, useEffect } from 'react';
import Transactions from '../components/list'
import Form from '../components/form'
import { getList } from '../service'

const Home = (props) => {
  const [list, setList] = useState(props.data)
  const [balance, setBalance] = useState(0)

  useEffect(() => {
    if(list) setBalance(list.reduce((total, { type, amount }) =>
      total - amount * (type === 'debit' ? 1 : -1), 0)
    )
  });

  async function refresh() {
    setList(false)
    const [res, err] = await getList()
    setList(!err ? res : []);
  }

  return (
    <main>
      <div>
        <h1>Current Balance <b> ${balance} </b> </h1>
      </div>
      <div>
        <h1>Add Transaction</h1>
        <Form {...{list, setList}} />
      </div>
      <div>
        <h1>Transaction History</h1>
        <button onClick={refresh}>Refresh list</button>
        <Transactions {...{list}} />
      </div>
    </main>
  ) 
}

Home.getInitialProps = async () => {
  const [res, err] = await getList()
	return { data: !err ? res : [] }
}

export default Home