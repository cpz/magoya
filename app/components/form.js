import { useState } from "react";
import { create } from '../service'

export default function Form({ setList, list }) {
  const empty = {
    type: 'debit',
    description: '',
    amount: 0
  }
  const [transaction, setTransaction] = useState(empty)
  const [loading, setLoading] = useState(false)

  const handleInputChange = evt => setTransaction({
    ...transaction,
    [evt.currentTarget.name]: evt.currentTarget.value
  })

  const handleSubmit = async evt => {
    evt.preventDefault();
    setLoading(true);
    let [res, err] = await create(transaction)
    setLoading(false)

    if(res.id) {
      setTransaction(empty)
      setList([...list, res])
    } else {
      alert(JSON.stringify(res || err))
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Type
        <select name="type" onChange={handleInputChange} value={transaction.type}>
          <option value="debit">Debit</option>
          <option value="credit">Credit</option>
        </select>
      </label>
      <label>
        Amount
        <input type="number" step="0.01" required name="amount" min="0.01"
          onChange={handleInputChange} value={transaction.amount} />
      </label>
      <label>
        Description
        <input type="text" required name="description"
          onChange={handleInputChange} value={transaction.description} />
      </label>
      <button className={loading?'loading':''} type="submit">Add transaction</button>
    </form>
  );
}