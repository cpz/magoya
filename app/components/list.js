
export default ({ list }) => {

  if (!list) return <div> Loading... </div>;

  return (
    <ul>
      <li>
        <div>Date</div>
        <div>Description</div>
        <div>Amount</div>
      </li>
      {list.map(t => (
        <li key={t.id} className={t.type}>
          <div>{ new Date(t.created).toLocaleString() }</div>
          <div>{t.description}</div>
          <div>{t.type==='debit'?'-':''} ${t.amount}</div>
        </li>
      ))}
    </ul>
  );
}