
Live version (http://magoya.cpz.now.sh/)

## Django REST API

```bash

# install requirements
pip install -r requirements.txt   
# migrate db
python manage.py migrate
# run server
python manage.py runserver

# http://localhost:8000/api/

```

## Next.js Frontend

```bash

cd app/

# install dependencies
yarn

# dev w/ live reload
yarn dev

# build / start
yarn build && yarn start

# http://localhost:3000

```
